export class Product {
    id!: string;
    name!: string;
    cost!: number;
    quantity!:number;
  }