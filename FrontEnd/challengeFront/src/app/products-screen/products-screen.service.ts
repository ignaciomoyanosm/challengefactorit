import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Product } from './product';
import { ProductPerCart } from './productPerCart';
import { Cost } from './cost';
import { Cart } from '../cart-screen/cart';

@Injectable({
  providedIn: 'root'
})
export class ProductsScreenService{
    private url:string="http://localhost:8082/";
    constructor(private http: HttpClient) {}

    getAll(): Observable<Product[]> {
        return this.http.get<Product[]>(this.url+"product");
    }

    addProductToCart(productPerCart:ProductPerCart):Observable<ProductPerCart>{
      return this.http.post<ProductPerCart>(this.url+"productpercart", productPerCart);
    }
    getCartStatus(cartId:String):Observable<Cost>{
      return this.http.get<Cost>(this.url+"cart/getStatus/"+cartId);
    }
    deleteProductOfCart(cartId:String, productId:String):Observable<ProductPerCart>{
      return this.http.delete<ProductPerCart>(this.url+"productpercart/detach/"+cartId+"/"+productId);
    }
    deleteCart(cartId:String):Observable<Cart>{
      return this.http.delete<Cart>(this.url+"cart/delete/"+cartId);
    }
}