import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsScreenService } from './products-screen.service';
import { Product } from './product';
import { ProductPerCart } from './productPerCart';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products-screen',
  templateUrl: './products-screen.component.html',
  styleUrls: ['./products-screen.component.scss']
})
export class ProductsScreenComponent implements OnInit{
  id!: string; // Propiedad para almacenar el ID
  quantity: number = 0
  products!: Product[];
  cartStatus: Number = 0;
  constructor(private route: ActivatedRoute,private productsScreenService: ProductsScreenService, private router: Router) { }

  ngOnInit() {
    // Obtén el valor del parámetro 'id' de la URL
    this.route.params.subscribe(params => {
      this.id = params['id']; // Asigna el valor del parámetro 'id' a la propiedad 'id'
    });
    this.productsScreenService.getAll().subscribe(
      p => this.products=p
    );
  }
  addToCart(product: Product) {
    product.quantity +=1;
    
    if (product.quantity > 0) {
      const productPerCart: ProductPerCart = {
        id: '',
        quantityOfProducts: product.quantity,
        cartId: this.id,
        productId: product.id
      };

      this.productsScreenService.addProductToCart(productPerCart).subscribe(
        response => {
          console.log('Producto agregado al carrito:', response);
        },
        error => {
          console.error('Error al agregar el producto al carrito:', error);
        }
      );
    } else {
      console.error('La cantidad debe ser mayor que cero.');
    }
  }
  restToCart(product: Product) {
    
    if ( product.quantity > 0) {
      product.quantity -= 1;
      const productPerCart: ProductPerCart = {
        id: '',
        quantityOfProducts:  product.quantity,
        cartId: this.id,
        productId: product.id
      };

      this.productsScreenService.addProductToCart(productPerCart).subscribe(
        response => {
          console.log('Producto agregado al carrito:', response);
        },
        error => {
          console.error('Error al agregar el producto al carrito:', error);
        }
      );
    } else {
      console.error('La cantidad debe ser mayor que cero.');
    }
  }
  getCartStatus() {
    this.productsScreenService.getCartStatus(this.id).subscribe(
      status => {
        this.cartStatus = status.totalCost;
      },
      error => {
        console.error('Error al obtener el estado del carrito:', error);
      }
    );
  }
  deleteProduct(product:Product){
    product.quantity = 0;
    this.productsScreenService.deleteProductOfCart(this.id, product.id);
    
  }
  deleteCart() {
    this.productsScreenService.deleteCart(this.id).subscribe(
      response => {
        console.log('Cart deleted successfully:', response);
        this.router.navigate(['/cart-form']);
      },
      error => {
        console.error('Error deleting cart:', error);
      }
    );
  }

}
