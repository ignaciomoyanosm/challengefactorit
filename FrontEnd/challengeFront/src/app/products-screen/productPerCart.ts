export class ProductPerCart{
    id! : String;
    quantityOfProducts! : Number;
    cartId! : String;
    productId! : String;
}