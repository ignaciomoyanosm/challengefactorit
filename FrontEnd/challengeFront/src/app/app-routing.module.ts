import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartScreenComponent } from './cart-screen/cart-screen.component';
import { FormCartComponent } from './cart-screen/cart-form.component';
import { ProductsScreenComponent } from './products-screen/products-screen.component';
import { UsersScreenComponent } from './users-screen/users-screen.component';

const routes: Routes = [
  {path:'cart-screen', component:CartScreenComponent},
  {path:'cart-form', component:FormCartComponent},
  {path:'add-products/:id', component:ProductsScreenComponent},
  {path:'users-screen', component:UsersScreenComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
