import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cart } from './cart';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CartScreenService{
    
    private url:string="http://localhost:8082/cart";
    constructor(private http: HttpClient) {}
    

    getAll(): Observable<Cart[]> {
        return this.http.get<Cart[]>(this.url);
    }

    create(cart:Cart):Observable<Cart>{
      return this.http.post<Cart>(this.url+"/new", cart);
    }
} 