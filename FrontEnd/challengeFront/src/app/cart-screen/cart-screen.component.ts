import { Component, OnInit, Inject } from '@angular/core';
import { Cart } from './cart';
import { CartScreenService } from './cart-screen.service';

@Component({
  selector: 'app-cart-screen',
  templateUrl: './cart-screen.component.html',
  styleUrls: ['./cart-screen.component.scss']
})
export class CartScreenComponent implements OnInit{

  carts!: Cart[];

  constructor(private cartScreenService : CartScreenService){
    
  }
  ngOnInit(): void {
    this.cartScreenService.getAll().subscribe(
      c => this.carts=c
    );
  }


}
