import { Component, ElementRef,ViewChild, OnInit } from '@angular/core';
import { Cart } from './cart';
import { ActivatedRoute, Router } from '@angular/router';
import { CartScreenService } from './cart-screen.service';
import { FormsModule } from '@angular/forms';

@Component({
    selector: 'cart-form-familia',
    templateUrl: './cart-form.component.html',
    styleUrls: ['./cart-form.component.css']
  })

export class FormCartComponent{
    @ViewChild('datePicker') datePicker!: ElementRef
    cart:Cart = new Cart();
    titulo:string="Cart creation";

    constructor(private cartScreenService:CartScreenService, private router:Router, private activatedRoute:ActivatedRoute){

    }
    obtenerFechaSeleccionada() {
      const fechaSeleccionada = this.datePicker.nativeElement.value;
      console.log('Fecha seleccionada:', fechaSeleccionada);
    }

    create():void{
        console.log(this.cart);
        this.cart.date = this.datePicker.nativeElement.value
        this.cartScreenService.create(this.cart).subscribe(
          rfa=>{
            const createdCartId = rfa.id;
            this.router.navigate(['/add-products', createdCartId])
          }
        );
      }

}