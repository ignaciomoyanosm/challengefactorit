import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UsersScreenService{
    
    private url:string="http://localhost:8082/user";
    constructor(private http: HttpClient) {}
    

    getAll(): Observable<User[]> {
        return this.http.get<User[]>(this.url);
    }
} 