import { Component } from '@angular/core';
import { User } from './user';
import { UsersScreenService } from './users-screen.service';

@Component({
  selector: 'app-users-screen',
  templateUrl: './users-screen.component.html',
  styleUrls: ['./users-screen.component.scss']
})
export class UsersScreenComponent {

  users!: User[];

  constructor(private usersScreenService : UsersScreenService){
    
  }
  ngOnInit(): void {
    this.usersScreenService.getAll().subscribe(
      u => this.users=u
    );
  }

}
