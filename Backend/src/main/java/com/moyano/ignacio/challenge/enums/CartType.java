package com.moyano.ignacio.challenge.enums;

import com.moyano.ignacio.challenge.config.Messages;
import com.moyano.ignacio.challenge.exceptions.runtimeExceptions.EnumException;
import org.springframework.util.StringUtils;

import java.util.TreeMap;

public enum CartType {
    BASIC("cart.type.basic"),
    SPECIAL_DATE("cart.type.special_date"),
    VIP_USER("cart.type.vip_user");
    private final String string;

    CartType(String string) {
        this.string = string;
    }

    public String getString() {
        return Messages.getMessageForLocale(this.string);
    }

    public static TreeMap<CartType, String> getAll() {
        TreeMap<CartType, String> cartTypes = new TreeMap<>();
        for(CartType el : CartType.values()) {
            cartTypes.put(el, el.getString());
        }
        return cartTypes;
    }

    public static CartType validateCartType(String cartTypeString) {
        if(!StringUtils.hasText(cartTypeString)) {
            return null;
        } else {
            try {
                return CartType.valueOf(cartTypeString);
            } catch (Exception ex) {
                throw new EnumException(cartTypeString);
            }
        }
    }
}
