package com.moyano.ignacio.challenge.repositories;

import com.moyano.ignacio.challenge.entities.SpecialDates;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;

@Repository
public interface SpecialDatesRepo extends JpaRepository<SpecialDates, String> {
    @Query(value = "SELECT CASE WHEN :date BETWEEN offer_dates.start_date AND offer_dates.end_date THEN true ELSE false END FROM offer_dates", nativeQuery = true)
    int isAnOfferDate(@Param("date") Date date);
}
