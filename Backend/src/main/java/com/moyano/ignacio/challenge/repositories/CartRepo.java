package com.moyano.ignacio.challenge.repositories;

import com.moyano.ignacio.challenge.entities.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CartRepo extends JpaRepository<Cart, String> {
    @Modifying
    @Query(value = "DELETE FROM cart WHERE id = :id", nativeQuery = true)
    void deleteCartTesting(@Param("id") String id);
}
