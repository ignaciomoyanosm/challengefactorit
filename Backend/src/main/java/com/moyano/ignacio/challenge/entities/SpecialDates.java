package com.moyano.ignacio.challenge.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import java.util.Date;
@Data
@Entity
@Table(name = "offer_dates")
@NoArgsConstructor
public class SpecialDates {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id", nullable = false, unique = true, length = 36)
    private String id;
    @Column
    private Date startDate;
    @Column
    private Date endDate;
}
