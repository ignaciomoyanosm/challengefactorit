package com.moyano.ignacio.challenge.dtos;

public interface EntityUpdatableFromDTO<T> {
    T updateEntity(T entity);
}
