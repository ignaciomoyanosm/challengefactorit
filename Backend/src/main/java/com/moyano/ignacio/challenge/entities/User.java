package com.moyano.ignacio.challenge.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

@Data
@Entity
@Table(name = "user")
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id", nullable = false, unique = true, length = 36)
    private String id;

    @Column(length = 60, nullable = false,unique = true)
    private String userName;

    @Column(nullable = false)
    private Boolean VIP;

    @Column
    private double totalSpendMonth;
}
