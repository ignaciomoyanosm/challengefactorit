package com.moyano.ignacio.challenge.repositories;
import com.moyano.ignacio.challenge.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserRepo extends JpaRepository<User, String> {

    @Modifying
    @Query("UPDATE User u SET u.totalSpendMonth = :totalSpendMonth WHERE u.id = :userId")
    void updateTotalSpendMonth(@Param("totalSpendMonth") Double totalSpendMonth, @Param("userId") String userId);

    @Query(value = "SELECT user.* FROM user WHERE user.vip = true", nativeQuery = true)
    List<User> getVipUsers();

    Optional<User> findByUserName(String userName);
}
