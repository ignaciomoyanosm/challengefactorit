package com.moyano.ignacio.challenge.controllers;

import com.moyano.ignacio.challenge.dtos.requests.ProductPerCartRequest;
import com.moyano.ignacio.challenge.dtos.responses.CartResponse;
import com.moyano.ignacio.challenge.dtos.responses.ProductPerCartResponse;
import com.moyano.ignacio.challenge.services.ProductPerCartService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("productpercart")
@RequiredArgsConstructor
@Tag(name = "Product Per Cart Controller")
public class ProductPerCartController {

    private final ProductPerCartService productPerCartService;
    @Operation(summary = "Add a product to a cart",
            description = "this operation register a product to a cart")
    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping
    public ResponseEntity<ProductPerCartResponse> addProductToCart(@Validated @RequestBody ProductPerCartRequest productPerCartRequest){
        return new ResponseEntity<>(productPerCartService.addProductToCart(productPerCartRequest), HttpStatus.CREATED);
    }

    @Operation(summary= "Delete a product from a cart",
            description = "this operation delete a product stored in the database of a cart")
    @DeleteMapping("/detach/{cartId}/{productId}")
    @CrossOrigin(origins = "http://localhost:4200")
    @Transactional
    public ResponseEntity<CartResponse> deleteProfileCandidate(
            @PathVariable String cartId, @PathVariable String productId) {
        return new ResponseEntity<>(productPerCartService.deleteProductFromCart(cartId, productId), HttpStatus.OK);
    }
}
