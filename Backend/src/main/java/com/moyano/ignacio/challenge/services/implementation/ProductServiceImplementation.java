package com.moyano.ignacio.challenge.services.implementation;

import com.moyano.ignacio.challenge.dtos.responses.CartResponse;
import com.moyano.ignacio.challenge.dtos.responses.ProductResponse;
import com.moyano.ignacio.challenge.entities.Product;
import com.moyano.ignacio.challenge.repositories.ProductRepo;
import com.moyano.ignacio.challenge.services.ProductService;
import com.moyano.ignacio.challenge.utils.EntityDtoConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductServiceImplementation implements ProductService {
    private final EntityDtoConverter converter;
    private final ProductRepo productRepo;

    public Product getProductById(String id) {
        Optional<Product> productOptional = productRepo.findById(id);
        if (productOptional.isPresent()) {
            return productOptional.get();
        } else {
            throw new RuntimeException("Product not found");
        }
    }

    @Override
    public List<ProductResponse> getAllProducts() {
        return converter.convertGenericEntityList(productRepo.findAll(), ProductResponse.class);
    }
}
