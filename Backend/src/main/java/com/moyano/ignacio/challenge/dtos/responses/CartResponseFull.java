package com.moyano.ignacio.challenge.dtos.responses;

import com.moyano.ignacio.challenge.entities.Product;
import com.moyano.ignacio.challenge.entities.ProductPerCart;
import com.moyano.ignacio.challenge.enums.CartType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Schema(name = "Cart Response Full")
public class CartResponseFull {
    private Double totalCost;
}
