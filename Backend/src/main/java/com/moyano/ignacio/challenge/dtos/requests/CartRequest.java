package com.moyano.ignacio.challenge.dtos.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.moyano.ignacio.challenge.dtos.EntityUpdatableFromDTO;
import com.moyano.ignacio.challenge.entities.Cart;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Schema(name = "Cart Request")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CartRequest implements EntityUpdatableFromDTO<Cart> {
    @NotBlank private String userName;
    @NotBlank Date date;
    private List<String> products;

    @Override
    public Cart updateEntity(Cart entity) {
        if(entity == null) return null;
        return entity;
    }
}
