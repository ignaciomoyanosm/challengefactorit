package com.moyano.ignacio.challenge.services.implementation;

import com.moyano.ignacio.challenge.dtos.requests.CartRequest;
import com.moyano.ignacio.challenge.dtos.requests.UserRequest;
import com.moyano.ignacio.challenge.dtos.responses.CartResponse;
import com.moyano.ignacio.challenge.dtos.responses.CartResponseFull;
import com.moyano.ignacio.challenge.dtos.responses.UserResponse;
import com.moyano.ignacio.challenge.entities.Cart;
import com.moyano.ignacio.challenge.entities.ProductPerCart;
import com.moyano.ignacio.challenge.entities.User;
import com.moyano.ignacio.challenge.enums.CartType;
import com.moyano.ignacio.challenge.repositories.CartRepo;
import com.moyano.ignacio.challenge.repositories.SpecialDatesRepo;
import com.moyano.ignacio.challenge.repositories.UserRepo;
import com.moyano.ignacio.challenge.services.CartService;
import com.moyano.ignacio.challenge.services.UserService;
import com.moyano.ignacio.challenge.utils.EntityDtoConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CartServiceImplementation implements CartService {

    private final EntityDtoConverter converter;
    private final CartRepo cartRepo;
    private final UserRepo userRepo;
    private final SpecialDatesRepo specialDatesRepo;
    private final UserService userService;

    @Transactional
    public List<CartResponse> getAllCarts() {
        return converter.convertGenericEntityList(cartRepo.findAll(), CartResponse.class);
    }

    public CartResponse newCart(CartRequest cartRequest) {
        Cart newCart = createCartFromRequest(cartRequest);
        setCartTypeBasedOnUserAndDate(newCart, cartRequest);
        Cart savedCart = saveCart(newCart);
        return convertCartToResponse(savedCart);
    }

    public Cart findById(String id) {
        Optional<Cart> cartOptional = cartRepo.findById(id);
        if (cartOptional.isPresent()) {
            return cartOptional.get();
        } else {
            throw new RuntimeException("Cart not found");
        }
    }

    @Transactional
    public CartResponse getById(String id) {
            return converter.convertGenericEntity(this.findById(id), CartResponse.class);
    }

    @Transactional
    public CartResponseFull getCartStatus(String id) {
        Cart cart = this.findById(id);
        return CartResponseFull.builder()
                .totalCost(getTotalCost(cart))
                .build();


    }

    @Transactional
    public CartResponse deleteCart(String id) {
        Optional<Cart> cart = cartRepo.findById(id);
        cartRepo.deleteCartTesting(id);
        return getById(id);
    }

    @Transactional
    private Double getTotalCost(Cart cart){
        double cost = 0;
        double totalProducts= 0;
        for (int i = 0; i < cart.getProducts().size(); i++) {
            cost += cart.getProducts().get(i).getProduct().getCost() * cart.getProducts().get(i).getQuantityOfProducts();
            totalProducts += 1 * cart.getProducts().get(i).getQuantityOfProducts();
        }
        cost = applyDiscount(cart, cost, totalProducts);
        uploadUserSpent(cart, cost);
        return cost;
    }

    private static double applyDiscount(Cart cart, double cost, double totalProducts) {
        if (totalProducts ==4){
            cost -= cost * 0.25;
        }
        if (totalProducts >10){
            cost = getDiscount(cart, cost);
        }
        return cost;
    }

    private void uploadUserSpent(Cart cart, double cost) {
        User user = cart.getUser();
        double aux = user.getTotalSpendMonth();
        aux += cost;
        user.setTotalSpendMonth(aux);
        if (user.getTotalSpendMonth()>1000){
            UserRequest userRequest = new UserRequest(cart.getUser().getUserName(), aux, true);
            userRequest.updateEntity(user);
        }
        UserRequest userRequest = new UserRequest(cart.getUser().getUserName(), aux, user.getVIP());
        userRequest.updateEntity(user);
        converter.convertGenericEntity(userRepo.saveAndFlush(user), UserResponse.class);
    }

    private static double getDiscount(Cart cart, double cost) {
        if (cart.getCartType()== CartType.VIP_USER){
            double minCost = Double.MAX_VALUE;
            List<ProductPerCart> products = cart.getProducts();
            for (ProductPerCart product : products){
                double productCost = product.getProduct().getCost();
                if (productCost < minCost) {
                    minCost = productCost;
                }
            }
            cost -=minCost;
            cost -=500;
        }
        if (cart.getCartType()== CartType.SPECIAL_DATE){
            cost -=300;
        }
        if (cart.getCartType()== CartType.BASIC){
            cost -=100;
        }
        return cost;
    }

    private Cart createCartFromRequest(CartRequest cartRequest) {
        Cart newCart = cartRequest.updateEntity(new Cart());
        User user = userService.getUserByUserName(cartRequest.getUserName());
        //User user = userService.getUserById(cartRequest.getUserId());
        newCart.setUser(user);
        return newCart;
    }

    private void setCartTypeBasedOnUserAndDate(Cart cart, CartRequest cartRequest) {
        User user = userService.getUserByUserName(cartRequest.getUserName());
        //User user = userService.getUserById(cartRequest.getUserId());
        System.out.println("-------------"+cartRequest.getDate()+"-------------");
        if (user.getVIP()) {
            cart.setCartType("VIP_USER");
        } else if (specialDatesRepo.isAnOfferDate(cartRequest.getDate()) == 1) {
            cart.setCartType("SPECIAL_DATE");
        } else {
            cart.setCartType("BASIC");
        }
    }

    private Cart saveCart(Cart cart) {
        return cartRepo.save(cart);
    }

    private CartResponse convertCartToResponse(Cart cart) {
        return converter.convertGenericEntity(cartRepo.saveAndFlush(cart), CartResponse.class);
    }
}
