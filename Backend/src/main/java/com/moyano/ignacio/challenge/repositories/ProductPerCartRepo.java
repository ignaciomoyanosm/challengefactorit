package com.moyano.ignacio.challenge.repositories;
import com.moyano.ignacio.challenge.entities.ProductPerCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ProductPerCartRepo extends JpaRepository<ProductPerCart, String> {
    ProductPerCart findByCartIdAndProductId(String cartId, String productId);

    @Query(value = "SELECT product_per_cart.* FROM product_per_cart WHERE product_per_cart.cart_id =:cartId AND product_per_cart.product_id =:productId ",nativeQuery = true)
    Optional <ProductPerCart> findByCartIdAndProductIdOptional(String cartId, String productId);
}
