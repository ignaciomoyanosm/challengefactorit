package com.moyano.ignacio.challenge.repositories;

import com.moyano.ignacio.challenge.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepo extends JpaRepository<Product, String> {
}
