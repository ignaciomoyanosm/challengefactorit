package com.moyano.ignacio.challenge.controllers;

import com.moyano.ignacio.challenge.dtos.requests.CartRequest;
import com.moyano.ignacio.challenge.dtos.responses.CartResponse;
import com.moyano.ignacio.challenge.dtos.responses.CartResponseFull;
import com.moyano.ignacio.challenge.services.CartService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("cart")
@RequiredArgsConstructor
@Tag(name = "Cart Controller")
public class CartController {

    private final CartService cartService;
    @Operation(summary = "Retrieve all carts", description = "this operation returns all carts")
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping
    public ResponseEntity<List<CartResponse>> getAllCarts() {
        return new ResponseEntity<>(cartService.getAllCarts(), HttpStatus.OK);
    }
    @Operation(summary = "Retrieve cart status", description = "this operation returns the status of a cart")
    @GetMapping("/getStatus/{cartId}")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<CartResponseFull> getCartStatus(
            @PathVariable String cartId
    ) {
        return new ResponseEntity<>(cartService.getCartStatus(cartId), HttpStatus.OK);
    }
    @Operation(summary = "new cart", description= "this operation registers a new cart")
    @PostMapping("/new")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<CartResponse> newCart(@Validated @RequestBody CartRequest cartRequest){
        return new ResponseEntity<>(cartService.newCart(cartRequest), HttpStatus.CREATED);
    }
    @Operation(summary = "new cart", description= "this operation registers a new cart")
    @DeleteMapping("/delete/{cartId}")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<CartResponse> deleteCart(@PathVariable String cartId){
        return new ResponseEntity<>(cartService.deleteCart(cartId), HttpStatus.CREATED);
    }
}
