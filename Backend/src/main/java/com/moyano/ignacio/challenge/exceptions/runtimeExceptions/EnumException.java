package com.moyano.ignacio.challenge.exceptions.runtimeExceptions;

public class EnumException extends RuntimeException {
    public EnumException() {
        super();
    }

    public EnumException(String message) {
        super(message);
    }

}
