package com.moyano.ignacio.challenge.dtos.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.moyano.ignacio.challenge.dtos.EntityUpdatableFromDTO;
import com.moyano.ignacio.challenge.entities.Cart;
import com.moyano.ignacio.challenge.entities.User;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Schema(name = "User Request")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserRequest implements EntityUpdatableFromDTO<User> {
    @NotBlank private String userName;
    private Double totalSpendMonth;
    private Boolean VIP;
    @Override
    public User updateEntity(User entity) {
        if(entity == null) return null;
        entity.setTotalSpendMonth(this.totalSpendMonth);
        entity.setVIP(this.VIP);
        return entity;
    }
}
