package com.moyano.ignacio.challenge.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.moyano.ignacio.challenge.enums.CartType;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.List;

@Data
@Entity
@Table(name = "cart")
@NoArgsConstructor
public class Cart {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id", nullable = false, unique = true, length = 36)
    private String id;
    @Enumerated(EnumType.STRING)
    private CartType cartType;
    @JsonIgnore
    @OneToMany(mappedBy = "cart", cascade = CascadeType.ALL)
    private List<ProductPerCart> products;
    @ManyToOne(optional = false)
    private User user;

    public void setCartType(String cartTypeString) {
        this.cartType = CartType.validateCartType(cartTypeString);
    }



}
