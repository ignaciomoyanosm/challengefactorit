package com.moyano.ignacio.challenge.dtos.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Schema(name = "ProductPerCart Response Custom")
public class ProductPerCartResponseCustom {
    private String productName;
    private String quantityOfProducts;
}
