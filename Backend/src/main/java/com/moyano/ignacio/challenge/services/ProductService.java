package com.moyano.ignacio.challenge.services;

import com.moyano.ignacio.challenge.dtos.responses.ProductResponse;
import com.moyano.ignacio.challenge.entities.Product;

import java.util.List;

public interface ProductService {
    Product getProductById(String id);

    List<ProductResponse> getAllProducts();
}
