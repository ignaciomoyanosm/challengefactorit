package com.moyano.ignacio.challenge.utils;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class EntityDtoConverter {

    private final ModelMapper modelMapper;

    //-----Generic Converters-----\\
    public <E,D> D convertGenericEntity(E entity, Class<D> DTOclass){
        return modelMapper.map(entity, DTOclass);
    }

    public <E,D> List<D> convertGenericEntityList(List<E> entities, Class<D> DTOclass){
        List<D> response = new ArrayList<>();
        for (E entity: entities) {
            response.add(convertGenericEntity(entity, DTOclass));
        }
        return response;
    }

}
