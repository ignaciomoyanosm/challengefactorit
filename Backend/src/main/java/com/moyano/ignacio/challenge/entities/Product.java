package com.moyano.ignacio.challenge.entities;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.List;

@Data
@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id", nullable = false, unique = true, length = 36)
    private String id;
    @Column(length = 60, nullable = false)
    private String name;
    @Column(nullable = false)
    private Long cost;
    @Column(nullable = false)
    private Long quantity;
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<ProductPerCart> productPerCart;
}
