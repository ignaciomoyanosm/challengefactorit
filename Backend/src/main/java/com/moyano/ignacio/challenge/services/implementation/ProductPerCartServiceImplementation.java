package com.moyano.ignacio.challenge.services.implementation;

import com.moyano.ignacio.challenge.dtos.requests.ProductPerCartRequest;
import com.moyano.ignacio.challenge.dtos.responses.CartResponse;
import com.moyano.ignacio.challenge.dtos.responses.ProductPerCartResponse;
import com.moyano.ignacio.challenge.entities.Cart;
import com.moyano.ignacio.challenge.entities.Product;
import com.moyano.ignacio.challenge.entities.ProductPerCart;
import com.moyano.ignacio.challenge.repositories.ProductPerCartRepo;
import com.moyano.ignacio.challenge.services.CartService;
import com.moyano.ignacio.challenge.services.ProductPerCartService;
import com.moyano.ignacio.challenge.services.ProductService;
import com.moyano.ignacio.challenge.utils.EntityDtoConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductPerCartServiceImplementation implements ProductPerCartService {

    private final EntityDtoConverter converter;
    private final CartService cartService;
    private final ProductService productService;
    private final ProductPerCartRepo productPerCartRepo;

    public ProductPerCartResponse addProductToCart(ProductPerCartRequest productPerCartRequest) {
        Cart cart = cartService.findById(productPerCartRequest.getCartId());
        Product product = productService.getProductById(productPerCartRequest.getProductId());
        Optional<ProductPerCart> result = productPerCartRepo.findByCartIdAndProductIdOptional(cart.getId(), product.getId());
        if (result.isPresent()) {
            ProductPerCart productPerCart = result.get();
            productPerCart.setQuantityOfProducts(productPerCartRequest.getQuantityOfProducts());
            productPerCartRepo.save(productPerCart);
            return converter.convertGenericEntity(productPerCart, ProductPerCartResponse.class);
        }
        ProductPerCart productPerCart = ProductPerCart.builder()
                .quantityOfProducts(productPerCartRequest.getQuantityOfProducts())
                .product(product)
                .cart(cart)
                .build();
        return converter.convertGenericEntity(productPerCartRepo.save(productPerCart), ProductPerCartResponse.class);
    }

    @Override
    public CartResponse deleteProductFromCart(String cartId, String productId) {
        ProductPerCart productPerCart = productPerCartRepo.findByCartIdAndProductId(cartId, productId);
        Cart cart = cartService.findById(cartId);
        cart.getProducts().remove(productPerCart);
        productPerCartRepo.delete(productPerCart);
        return cartService.getById(cartId);
    }
}
