package com.moyano.ignacio.challenge.dtos.responses;

import com.moyano.ignacio.challenge.entities.ProductPerCart;
import com.moyano.ignacio.challenge.enums.CartType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Schema(name = "Cart Response")
public class CartResponse {
    private String id;
    private CartType cartType;
    private List<ProductPerCartResponse> products;
}
