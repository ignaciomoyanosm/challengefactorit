package com.moyano.ignacio.challenge.exceptions.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Builder
@AllArgsConstructor
@Schema(name = "Exception Response")
public class ExceptionResponse {

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Timestamp timestamp;
    private int status;
    private String error;
    private String message;
    private String path;
}
