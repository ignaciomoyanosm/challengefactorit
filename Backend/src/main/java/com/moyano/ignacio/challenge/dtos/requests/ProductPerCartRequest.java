package com.moyano.ignacio.challenge.dtos.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Schema(name = "ProdcutPerCart Request")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductPerCartRequest {
    @NotBlank String productId;
    @NotBlank String cartId;
    @NotBlank Integer quantityOfProducts;
}
