package com.moyano.ignacio.challenge.controllers;

import com.moyano.ignacio.challenge.dtos.responses.CartResponse;
import com.moyano.ignacio.challenge.dtos.responses.UserResponse;
import com.moyano.ignacio.challenge.services.CartService;
import com.moyano.ignacio.challenge.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("user")
@RequiredArgsConstructor
@Tag(name = "users Controller")
public class UserController {

    private final UserService userService;

    @Operation(summary = "Retrieve all vip users", description = "this operation returns all vip users")
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping
    public ResponseEntity<List<UserResponse>> getVipUsers() {
        return new ResponseEntity<>(userService.getVIPUsers(), HttpStatus.OK);
    }
}
