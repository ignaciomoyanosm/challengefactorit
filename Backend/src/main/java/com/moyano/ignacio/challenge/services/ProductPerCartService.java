package com.moyano.ignacio.challenge.services;

import com.moyano.ignacio.challenge.dtos.requests.ProductPerCartRequest;
import com.moyano.ignacio.challenge.dtos.responses.CartResponse;
import com.moyano.ignacio.challenge.dtos.responses.ProductPerCartResponse;

public interface ProductPerCartService {

    ProductPerCartResponse addProductToCart(ProductPerCartRequest productPerCartRequest);

    CartResponse deleteProductFromCart(String cartId, String productId);
}
