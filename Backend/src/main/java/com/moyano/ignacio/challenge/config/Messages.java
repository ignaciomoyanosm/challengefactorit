package com.moyano.ignacio.challenge.config;

import org.springframework.context.i18n.LocaleContextHolder;

import java.util.ResourceBundle;

public class Messages {

    public static String getMessageForLocale(String messageKey) {
        return ResourceBundle.getBundle("messages", LocaleContextHolder.getLocale()).getString(messageKey);
    }

}
