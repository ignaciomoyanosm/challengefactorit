package com.moyano.ignacio.challenge.services.implementation;

import com.moyano.ignacio.challenge.dtos.responses.UserResponse;
import com.moyano.ignacio.challenge.entities.User;
import com.moyano.ignacio.challenge.repositories.UserRepo;
import com.moyano.ignacio.challenge.services.UserService;
import com.moyano.ignacio.challenge.utils.EntityDtoConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImplementation implements UserService {

    private final EntityDtoConverter converter;
    private final UserRepo userRepo;
    @Transactional
    public User getUserById(String id) {
        Optional<User> userOptional = userRepo.findById(id);
        if (userOptional.isPresent()) {
            return userOptional.get();
        } else {
            throw new RuntimeException("User not found");
        }
    }

    @Transactional
    public User getUserByUserName(String userName) {
        Optional<User> userOptional = userRepo.findByUserName(userName);
        if (userOptional.isPresent()) {
            return userOptional.get();
        } else {
            throw new RuntimeException("User not found");
        }
    }

    @Transactional
    public List<UserResponse> getVIPUsers() {
        return converter.convertGenericEntityList(userRepo.getVipUsers(), UserResponse.class);
    }
}
