package com.moyano.ignacio.challenge.dtos.responses;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Schema(name = "Cart Response")
public class ProductResponse {
    private String id;
    private String name;
    private Long cost;
    private Long quantity;
}
