package com.moyano.ignacio.challenge.services;

import com.moyano.ignacio.challenge.dtos.requests.CartRequest;
import com.moyano.ignacio.challenge.dtos.responses.CartResponse;
import com.moyano.ignacio.challenge.dtos.responses.CartResponseFull;
import com.moyano.ignacio.challenge.entities.Cart;

import java.util.List;

public interface CartService {
    List<CartResponse> getAllCarts();
    CartResponse newCart(CartRequest candidateRequest);
    Cart findById(String id);
    CartResponse getById(String id);
    CartResponseFull getCartStatus(String id);
    CartResponse deleteCart(String id);
}
