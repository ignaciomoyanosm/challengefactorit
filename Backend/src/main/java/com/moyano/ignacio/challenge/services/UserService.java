package com.moyano.ignacio.challenge.services;
import com.moyano.ignacio.challenge.dtos.responses.UserResponse;
import com.moyano.ignacio.challenge.entities.User;

import java.util.List;


public interface UserService {

    User getUserById(String id);

    User getUserByUserName(String userName);

    List<UserResponse> getVIPUsers();
}
